/**
 * Google Analytics plugin
 */

import videojs from 'videojs'

// Default options for the plugin.
const defaults = {
  eventsToTrack: [
    'player_load', 
    'video_load', 
    'percent_played', 
    'start', 
    'end', 
    'seek',
    'seek_start',
    'seek_end',
    'play', 
    'pause', 
    'resize', 
    'volume_change', 
    'error', 
    'fullscreen'
  ],
  eventCategory: 'Video Brightcove',
  eventNames: {
    "video_load": "Video Load",
    "percent_played": "Percent played",
    "start": "Media Begin",
    "seek_start": "Seek start",
    "seek_end": "Seek end",
    "play": "Media Play",
    "pause": "Media Pause",
    "error": "Error",
    "fullscreen_exit": "Fullscreen Exited",
    "fullscreen_enter": "Fullscreen Entered",
    "resize": "Resize",
    "volume_change": "Volume Change",
    "player_load": "Player Load",
    "end": "Media Complete"
  },
  eventLabel: false,
  percentsPlayedInterval: 10,
  tracker: false,
  sendbeaconOverride: false,
  debug: false
};

let init = [];
let trackers = {};

const detectTrackers = function() {
  if (typeof window.ga === "function") {
    window.ga(function() {
        let _trackers = window.ga.getAll();
        _trackers.forEach(function(tracker) {
          videojs.log('detectTrackers', tracker.get('trackingId'), tracker.get('name'));
          trackers[tracker.get('trackingId')] = tracker;
        })
    })
  }
}

const onPlayerReady = function (player, options) {
  let dataSetupOptions = {}, parsedOptions;
  if (player.options_["data-setup"]) {
    parsedOptions = JSON.parse(player.options_["data-setup"]);
    if (parsedOptions.ga) {
      dataSetupOptions = parsedOptions.ga;
    }
  }
  // Merge options from data-setup
  let options_ = videojs.mergeOptions(defaults, dataSetupOptions)
  options = videojs.mergeOptions(options_, options || {})

  // Get from html if not yet set in option
  options.playerId = options.playerId || player.el_.getAttribute('data-player')

  if (options.debug)
    videojs.log("Plugin ga > Final options", options)

  let getTrackerMethodName = function(method) {
    if (options.tracker && typeof trackers[options.tracker] === 'object') {
      let trackerName = trackers[options.tracker].get('name')
      method = trackerName + '.' + method
    }

    return method
  }

  let debug = function(str, params) {
    if (options.debug) {
      videojs.log(str, params);
    }
  }

  let adStateRegex,
    currentVideo,
    defaultLabel,
    end,
    endTracked,
    error, 
    eventLabel, 
    eventNames, 
    fullscreen, 
    getEventName, 
    href, 
    iframe, 
    isInAdState, 
    loaded,
    pause, 
    percentsAlreadyTracked, 
    play, 
    referrer, 
    resize, 
    seekEnd, 
    seekStart, 
    seeking, 
    sendbeacon, 
    sendbeaconOverride, 
    start, 
    startTracked,
    timeupdate,
    tracker,
    volumeChange,
    gaSend,
    gaPush;

    referrer = document.createElement('a');
    referrer.href = document.referrer;
    if (self !== top && window.location.host === 'preview-players.brightcove.net' && (referrer.hostname = 'studio.brightcove.com')) {
      videojs.log('Google analytics plugin will not track events in Video Cloud Studio');
      return;
    }
    defaultLabel = '';//options.eventLabel
    sendbeaconOverride = options.sendbeaconOverride
    percentsAlreadyTracked = [];
    startTracked = false;
    endTracked = false;
    seekStart = seekEnd = 0;
    seeking = false;
    eventLabel = '';
    currentVideo = '';
    eventNames = options.eventNames;

    // Map between key and human-readable event name
    getEventName = function(name) {
      if (options.eventNames && options.eventNames[name]) {
        return options.eventNames[name];
      }
      if (dataSetupOptions.eventNames && dataSetupOptions.eventNames[name]) {
        return dataSetupOptions.eventNames[name];
      }
      if (eventNames[name]) {
        return eventNames[name];
      }
      return name;
    };

    if (window.location.host === 'players.brightcove.net' || window.location.host === 'preview-players.brightcove.net') {
      tracker = options.tracker || dataSetupOptions.tracker;
      if (tracker) {
        (function(i, s, o, g, r, a, m) {
          i["GoogleAnalyticsObject"] = r;
          i[r] = i[r] || function() {
            return (i[r].q = i[r].q || []).push(arguments);
          };
          i[r].l = 1 * new Date();
          a = s.createElement(o);
          m = s.getElementsByTagName(o)[0];
          a.async = 1;
          a.src = g;
          return m.parentNode.insertBefore(a, m);
        })(window, document, "script", "//www.google-analytics.com/analytics.js", "ga");
        ga('create', tracker, 'auto');
        ga('require', 'displayfeatures');
      }
    }
    
    let setEventLabel = function() {
      if (options.eventLabel !== defaults.eventLabel) {
        eventLabel = options.eventLabel
      } else {
        if (player.mediainfo && player.mediainfo.id) {
          let eventLabelVars = []

          eventLabelVars.push(player.mediainfo.id)
          eventLabelVars.push(player.mediainfo.name)

          if (options.playerId) {
              eventLabelVars.push(options.playerId)
          }

          if (options.playerName) {
              eventLabelVars.push(options.playerName)
          }

          eventLabel = eventLabelVars.join(' | ')
        } else {
          eventLabel = player.currentSrc().split("/").slice(-1)[0].replace(/\.(\w{3,4})(\?.*)?$/i, '')
        }
      }
    }

    let register = function() {
      player.on("loadedmetadata", loaded);
      player.on("timeupdate", timeupdate);
      if (options.eventsToTrack.indexOf("end") >= 0) {
        player.on("ended", end);
      }
      if (options.eventsToTrack.indexOf("play") >= 0) {
        player.on("play", play);
      }
      if (options.eventsToTrack.indexOf("start") >= 0) {
        player.on("playing", start);
      }
      if (options.eventsToTrack.indexOf("pause") >= 0) {
        player.on("pause", pause);
      }
      if (options.eventsToTrack.indexOf("volume_change") >= 0) {
        player.on("volumechange", volumeChange);
      }
      if (options.eventsToTrack.indexOf("resize") >= 0) {
        player.on("resize", resize);
      }
      if (options.eventsToTrack.indexOf("error") >= 0) {
        player.on("error", error);
      }
      if (options.eventsToTrack.indexOf("fullscreen") >= 0) {
        return player.on("fullscreenchange", fullscreen);
      }
    }

    adStateRegex = /(\s|^)vjs-ad-(playing|loading)(\s|$)/;

    isInAdState = function(player) {
      return adStateRegex.test(player.el().className);
    };

    loaded = function() {
      //if (!isInAdState(player)) {
        if (player.mediainfo && player.mediainfo.id && player.mediainfo.id !== currentVideo) {
          currentVideo = player.mediainfo.id;
          percentsAlreadyTracked = [];
          startTracked = false;
          endTracked = false;
          seekStart = seekEnd = 0;
          seeking = false;
          if (options.eventsToTrack.indexOf("video_load") >= 0) {
            sendbeacon(getEventName('video_load'), true);
          }
        }
      //}
    };

    timeupdate = function() {
      var currentTime, duration, percent, percentPlayed, _i;
      if (!isInAdState(player)) {
        currentTime = Math.round(player.currentTime());
        duration = Math.round(player.duration());
        percentPlayed = Math.round(currentTime / duration * 100);
        for (percent = _i = 0; _i <= 99; percent = _i += options.percentsPlayedInterval) {
          if (percentPlayed >= percent && percentsAlreadyTracked.indexOf(percent) < 0) {
            if (options.eventsToTrack.indexOf("percent_played") >= 0 && percentPlayed !== 0) {
              sendbeacon(getEventName('percent_played'), true, percent);
            }
            if (percentPlayed > 0) {
              percentsAlreadyTracked.push(percent);
            }
          }
        }
        if (options.eventsToTrack.indexOf("seek") >= 0) {
          seekStart = seekEnd;
          seekEnd = currentTime;
          if (Math.abs(seekStart - seekEnd) > 1) {
            seeking = true;
            sendbeacon(getEventName('seek_start'), false, seekStart);
            sendbeacon(getEventName('seek_end'), false, seekEnd);
          }
        }
      }
    };

    end = function() {
      if (!isInAdState(player) && !endTracked) {
        sendbeacon(getEventName('end'), true);
        endTracked = true;
      }
    };

    play = function() {
      var currentTime;
      if (!isInAdState(player)) {
        currentTime = Math.round(player.currentTime());
        sendbeacon(getEventName('play'), true, currentTime);
        seeking = false;
      }
    };

    start = function() {
      if (!isInAdState(player)) {
        if (options.eventsToTrack.indexOf("start") >= 0 && !startTracked) {
          sendbeacon(getEventName('start'), true);
          return startTracked = true;
        }
      }
    };

    pause = function() {
      var currentTime, duration;
      if (!isInAdState(player)) {
        currentTime = Math.round(player.currentTime());
        duration = Math.round(player.duration());
        if (currentTime !== duration && !seeking) {
          sendbeacon(getEventName('pause'), true, currentTime);
        }
      }
    };
    
    volumeChange = function() {
      var volume;
      volume = player.muted() === true ? 0 : player.volume();
      sendbeacon(getEventName('volume_change'), false, volume);
    };

    resize = function() {
      sendbeacon(getEventName('resize') + ' - ' + player.width() + "*" + player.height(), true);
    };

    error = function() {
      var currentTime;
      currentTime = Math.round(player.currentTime());
      sendbeacon(getEventName('error'), true, currentTime);
    };

    fullscreen = function() {
      var currentTime;
      currentTime = Math.round(player.currentTime());
      if ((typeof player.isFullscreen === "function" ? player.isFullscreen() : void 0) || (typeof player.isFullScreen === "function" ? player.isFullScreen() : void 0)) {
        sendbeacon(getEventName('fullscreen_enter'), false, currentTime);
      } else {
        sendbeacon(getEventName('fullscreen_exit'), false, currentTime);
      }
    };

    gaSend = function(eventCategory, eventAction, eventLabel, eventValue, nonInteraction) {
      if (typeof window.ga === undefined) {
        if (options.debug) {
          videojs.log("Google Analytics is not available");
        }
        return
      }
        
      let params = {
        'eventCategory': eventCategory,
        'eventAction': eventAction,
        'eventLabel': eventLabel,
        'eventValue': eventValue,
        'nonInteraction': nonInteraction
      }
      let method = getTrackerMethodName('send')
      debug(method + ' > event', params)
      ga(method, 'event', params);
    }

    gaPush = function(eventCategory, eventAction, eventLabel, eventValue, nonInteraction) {
      if (typeof window._gaq === undefined) {
        if (options.debug) {
          videojs.log("Google Analytics is not available");
        }
        return
      }

      let params = {
        'eventCategory': eventCategory,
        'eventAction': eventAction,
        'eventLabel': eventLabel,
        'eventValue': eventValue,
        'nonInteraction': nonInteraction
      }
      debug('_gaq.push > _trackEvent', params)
      _gaq.push(['_trackEvent', eventCategory, eventAction, eventLabel, eventValue, nonInteraction])
    }

    sendbeacon = function(action, nonInteraction, value) {
      setEventLabel()
      if (sendbeaconOverride) {
        sendbeaconOverride(options.eventCategory, action, eventLabel, value, nonInteraction)
      } else if (window.ga) {
        gaSend(options.eventCategory, action, eventLabel, value, nonInteraction)
      } else if (window._gaq) {
        gaPush(options.eventCategory, action, eventLabel, value, nonInteraction)
      } else {
        debug("Google Analytics not detected");
      }
    }

    // Get document href to set as event label, since at this moment we do not have media info
    if (options.eventsToTrack.indexOf("player_load") >= 0) {
      if (self !== top) {
        href = document.referrer;
        iframe = 1;
      } else {
        href = window.location.href;
        iframe = 0;
      }
    }

    if (sendbeaconOverride) {
      sendbeaconOverride(options.eventCategory, getEventName('player_load'), href, iframe, true)
    } else if (window.ga) {
      gaSend(options.eventCategory, getEventName('player_load'), href, iframe, true)
    } else if (window._gaq) {
      gaPush(options.eventCategory, getEventName('player_load'), href, iframe, true)
    } else {
      debug("Google Analytics not detected");
    }

  // Register event handlers
  register()
}

const gaFunc = function (options) {
  // Get tracker list
  detectTrackers()  
  this.ready(() => {
    onPlayerReady(this, options);
  })
}

videojs.registerPlugin('ga', gaFunc);

//export default ga
