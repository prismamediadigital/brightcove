import webpack from 'webpack'
import path from 'path'
import BabiliPlugin from 'babili-webpack-plugin'
import ExtractTextPlugin from 'extract-text-webpack-plugin'

const configWebpackProd = {
  context: path.resolve(__dirname, 'app'),
  entry: {
    videojs: './js/videojs.js', // used only for local/dev
    autoplay: './js/autoplay.js',
    sticky: './js/sticky.js',
    ga: './js/ga.js',
    charity: './js/charity.js',
    logo: './js/logo.js',
    social: './js/initSocial.js',
    unmute: './js/unmuteDesktop.js',
    scroll2play: './js/scrollIntoView.js',
    i18nFR: './js/i18n/fr.js'
  },
  output: {
    filename: 'js/[name]-plugin.js',
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/',
    jsonpFunction: 'pmBrightcove',
    library: 'pmBrightcove',
    libraryTarget: 'umd',
    libraryExport: 'default'
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /(node_modules|bower_components)/,
      use: {
        loader: 'babel-loader'
      }
    }, {
      test: /\.css$/,
      exclude: /node_modules/,
      use: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: [{
          loader: 'css-loader',
          options: {
            importLoaders: 1,
            minimize: true || { /* CSSNano Options */ }
          }
        },
        {
          loader: 'postcss-loader'
        }]
      })
    }]
  },
  plugins: [
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify(process.env.NODE_ENV)
      }
    }),
    new webpack.optimize.ModuleConcatenationPlugin(),
    new ExtractTextPlugin({
      filename: 'css/[name]-plugin.css'
    }),
    new BabiliPlugin()
    // new WebpackMonitor({
    //   launch: true, // -> default 'false'
    //   port: 3030, // default -> 8081
    // })
  ],
  resolve: {
    modules: ['node_modules'],
    extensions: ['.js', '.css']
  },
  externals: {
    googletag: 'googletag',
    'googletag.cmd': 'googletag.cmd',
    pmdAdvertisingConfig: 'pmdAdvertisingConfig',
    dataLayer: 'dataLayer',
    gtmDataLayer: 'gtmDataLayer',
    videojs: 'videojs'
  },
  performance: {
    hints: 'warning'
  }
}

export default configWebpackProd
