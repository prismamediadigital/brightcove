(function () {
  var domaine = window.location.hostname
  var siteName = '';
  var target = '';
  var position = '';
  var pathName = window.location.pathname;
  var domaineok = 1;
  var pasDoublon = document.getElementById("charity");

  var closestByClass = function (el, className) {
    while (el.className.indexOf(className) === -1) {
      el = el.parentNode;
      if (!el) {
        return null
      }
    }
    return el
  }
  try {
    switch (domaine) {
      /* VOICI */
      case 'www.voici.fr':
        siteName = document.querySelector('meta[property="og:site_name"]').getAttribute('content');
        target = closestByClass(document.querySelector('.video-js'), 'p-r swiper-wrapper')
        break;
      /* GALA */
      case 'www.gala.fr':
        siteName = document.querySelector('meta[property="og:site_name"]').getAttribute('content');
        target = closestByClass(document.querySelector('.video-js'), 'media-flex-16-9')

        try {
          var miniature = document.getElementsByClassName('jumbo-thumbnails text-center');
          miniature[0].setAttribute("style", "margin-top :-30px;");
        }
        catch (e) { }

        break;
      /* TVGrandesChaines */
      case 'www.cesoirtv.com':
        siteName = 'CeSoirTv';
        target = closestByClass(document.querySelector('.video-js'), 'video_partenaire')
        break;
      /* FAC */
      case 'www.femmeactuelle.fr':
        siteName = document.querySelector('meta[property="og:site_name"]').getAttribute('content');
        target = closestByClass(document.querySelector('.video-js'), 'slider')
        break;
      /* CAC */
      case 'www.cuisineactuelle.fr':
        siteName = document.querySelector('meta[property="og:site_name"]').getAttribute('content');
        target = closestByClass(document.querySelector('.video-js'), 'slider')
        break;
      /* GEO */
      case 'www.geo.fr':
        siteName = document.querySelector('meta[property="og:site_name"]').getAttribute('content');
        target = closestByClass(document.querySelector('.video-js'), ' bg-black videoplayer')
        position = ' margin-bottom: 10px;';
        break;
      /* PRIMA */
      case 'www.prima.fr':
        siteName = document.querySelector('meta[property="og:site_name"]').getAttribute('content');
        target = document.getElementById('vjs_video_3');
        position = 'position: relative; top:26px; margin-bottom:25px;';
        break;
      /* Télé2Semaines */
      case 'www.programme.tv':
        siteName = 'Programme.TV';
        target = closestByClass(document.querySelector('.video-js'), 'video_partenaire')
        break;
      /* Télé2Semaines MOBILE */
      case 'mobile.programme.tv':
        siteName = 'Programme.TV';
        target = closestByClass(document.querySelector('.video-js'), 'video_partenaire')
        break;
      /* Télé Loisirs */
      case 'www.programme-tv.net':
        siteName = 'Télé Loisirs';
        if (pathName.indexOf('/programme/') != -1) {
          /* BANDE ANNONCE */
          target = closestByClass(document.querySelector('.video-js'), 'video_partenaire');
        } else {
          /* NEWS TV */
          target = closestByClass(document.querySelector('.video-js'), 'p-r bgc-darker');
        }
        break;
      case 'm.programme-tv.net':
        siteName = 'Télé Loisirs';
        if (pathName.indexOf('/programme/') != -1) {
          /* BANDE ANNONCE MOBILE */
          target = closestByClass(document.querySelector('.video-js'), 'video_partenaire');

          var parent = document.getElementsByClassName('fiche-visuel');
          parent[0].setAttribute("style", "height :100%;");

          var information = document.getElementsByClassName('information');
          information[0].setAttribute("style", "bottom :80px;");
        } else {
          /* NEWS TV MOBILE */
          target = closestByClass(document.querySelector('.video-js'), 'video_partenaire');

          var parent = document.getElementsByClassName('news-detail-visuel video');
          parent[0].setAttribute("style", "height :100%;");
        }
        break;
      /* DEFAULT */
      default:
        domaineok = 0;
        break;
    }
    if (domaineok && pasDoublon == null) {
      var msg = '<div id="charity" style="font-family:arial;background:black; color:white; padding: 0 10px; ' + position + '"<p><b>' + siteName + ' s\'engage. En regardant cette vidéo, vous contribuez à la recherche médicale. </b><i><a href="http://www.prismamedia.com/prisma-media-sengage-aupres-de-linstitut-curie/" target="_blank" style="color:white;">Cliquez ici pour en savoir plus</a></i></p>'

      target.insertAdjacentHTML('afterEnd', msg)
    }
  }
  catch (e) { }
})()
