function autoPlayProm (player) {
  player.play().then(() => {
    console.log('Player: Autoplay successful')
  }).catch(function (error) {
    console.log('Player: Autoplay failed - ', error.message)
    player.muted(true)
    player.play()
    console.log('Player: Player mute and autoplay')
  })
}

export default autoPlayProm
