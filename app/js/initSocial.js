import videojs from 'videojs'

// Default options for the plugin.
const defaults = {
  ratio: "16/3", // or "4/3"
  iframe: true,
  domain: 'players.brightcove.net',
  services: {
    "facebook": true,
    "google": true,
    "twitter": true,
    "tumblr": true,
    "pinterest": true,
    "linkedin": true
  }
};

let init = [];
const onPlayerReady = function (player, options) {
  if (typeof player.social !== "function") {
    console.log("Plugin social is not registered")
    return
  }

  let playerOptions = player.options()

  let embedCode
  if (options.iframe) {
    let divStyle = "position: relative;"
    divStyle += "height: 0;"
    divStyle += "overflow: hidden;"
    if (options.ratio == "4/3") {
      divStyle += "padding-bottom: 75%;"
    } else {
      divStyle += "padding-bottom: 56.25%;"
    } 
    
    let iframeStyle = "position: absolute;"
    iframeStyle += "top:0;"
    iframeStyle += "left: 0;"
    iframeStyle += "width: 100%;"
    iframeStyle += "height: 100%;"

    embedCode = "<div style='" + divStyle + "'><iframe src='//" + options.domain + "/" + playerOptions['data-account'] + "/" + playerOptions['data-player'] + '_' + playerOptions['data-embed'] + "/index.html?videoId=" + playerOptions['data-video-id'] + "' allowfullscreen style='" + iframeStyle + "'></iframe></div>"
  } else {
    embedCode = "<div style='padding-top: 56.25%;'>" +
    "<video " +
    "style='width: 100%; height: 100%; position: absolute; top: 0px; bottom: 0px; right: 0px; left: 0px;' " + 
    "data-video-id='" + playerOptions['data-video-id'] + "' " +
    "data-account='" + playerOptions['data-account'] + "' " +
    "data-player='" + playerOptions['data-player'] + "' "+
    "data-embed='" + playerOptions['data-embed'] + "' "+
    "class='video-js' " +
    "controls " +
    "></video>" + 
    "</div> " + 
    "<scr" + "ipt src='//players.brightcove.net/" + playerOptions['data-account'] + "/" + playerOptions['data-player'] + "_" + playerOptions['data-embed'] + "/index.min.js'></scr" + "ipt>"
  }

  let socialOptions = {
      "embedCode": embedCode,
      "services": options.services
  }

  player.social(socialOptions)
}

const initSocialFunc = function (options) {
  this.ready(() => {
    onPlayerReady(this, videojs.mergeOptions(defaults, options || {}));
  })
}

videojs.registerPlugin('initSocial', initSocialFunc);
