/**
 * Plugin to initialize ima3 serverUrl
 * Uses config in data attribute data-tags
 * Example :
 * data-tags='{
            "adUnit": "/228216569/TeleLoisirs/News/Tele-Realite/Player_video_Leader",
            "typePage": "Text+video",
            "positionPage": "Leader",
            "partnerName": "wetv",
            "playerStart": "Autoplay",
            "pubStart": "onPlay"
        }'
 * The plugin must be initialized in brightcove backoffice
 * the conf object to pass is
 * {cmsId: XXXX}
 * 
 * The default values to use are :
 * 1969	Entertainment (TEL / PTV / CSTV / VSD)
 * 2089	Eco Découverte (CAP / NEON / NATGEO / HBR / CAM)
 * 2209	Féminin/People (CAC / HC / VOI / GAL / FAC)
 */

import 'videojs'
// Default options for the plugin.
const defaults = {
  adUnit: false,
  typePage: false,
  positionPage: false,
  playerId: false,
  partnerName: false,
  playerStart: false,
  pubStart: false,
  domainName: false,
  urlPrisma: false,
  cmsId: false,
  debug: false
};
// Options for embed Article
const embed = {
  typePage: "Text+Video",
  positionPage: "Embed",
  partnerName: false,
  playerStart: "Click",
  pubStart: "onPlay"
};

// window.prisma_vidcoin_tag = window.prisma_vidcoin_tag || ''

const initIma3 = function(options, player) {
  let el = player.el()
  let serverUrl = `https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=${options.adUnit}&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&url=[referrer_url]&description_url=${options.domainName}&correlator=[timestamp]&cust_params=tags%3D{mediainfo.tags}%26typepage%3D${options.typePage}%26PositionPage%3D${options.positionPage}%26PlayerID%3D${options.playerId}%26PartnerName%3D${options.partnerName}%26PlayerStart%3D${options.playerStart}%26PubStart%3D${options.pubStart}%26urlPrisma%3D${options.urlPrisma}&vid={mediainfo.id}&vpos=preroll&cmsid=${options.cmsId}`

  options.playerId = el.getAttribute('data-player')
  options.urlPrisma = encodeURIComponent(window.location.pathname)
  options.domainName = encodeURIComponent(window.location.origin)
  /**
   * If ima3 is defined as Plugin
   */
  if (typeof player.ima3 === "object") {
    player.ima3.settings.serverUrl = serverUrl;
  } else {
    window.prisma_vidcoin_tag = serverUrl;
    player.vidcoin({
        "appId": "558126881008389",
        "placementCodes": [
            "o5509t2mmw0kock0ssk8wko0gkk8sww"
        ],
        "player": {
            "autoplay": true
        },
        "customData": {
            "providers": {
                "vast": {
                    "tagUri": "[JSVAR:prisma_vidcoin_tag]"
                }
            }
        }
    })
    
  }

  player.integral({
    'anId': options.anId,
    'campId': options.campId,
    'placementId': `${PMDGdfp.taggingConfig.domain_name}/${PMDGdfp.taggingConfig.pages[0].label}/Player_video_${options.positionPage}`
  })

  if (options.debug) {
    console.log('>>> ADTAG options', options)
    console.log('>>> ADTAG serverUrl', serverUrl)
    console.log('>>> IAS Tag', `${PMDGdfp.taggingConfig.domain_name}/${PMDGdfp.taggingConfig.pages[0].label}/Player_video_${options.positionPage}`)
  }

}

const initDfp = (options) => {
  options.domainName = PMDGdfp.taggingConfig.domain_name
  options.adUnit = `/${PMDGdfp.taggingConfig.network_id}/${PMDGdfp.taggingConfig.domain_name}/${PMDGdfp.taggingConfig.pages[0].label}/Player_video_${options.positionPage}`

  return options
}

let init = [];
const onPlayerReady = function(player, options) {
  options = videojs.mergeOptions(defaults, options || {});

  let newServerUrl = true
  let el = player.el()
  if (el.hasAttribute('data-tags')) {
    options = videojs.mergeOptions(options, JSON.parse(el.getAttribute('data-tags')))
  }
  else if (el.hasAttribute('data-provider')) {
    options = videojs.mergeOptions(options, embed);
    options.partnerName = el.getAttribute('data-provider')
  }
  else {
    newServerUrl = false
  }

  
  if(newServerUrl){
    let listenEvent = true

    if( typeof window.PMDGdfp !== 'undefined' && typeof window.PMDGdfp.taggingConfig !== 'undefined' ){
      listenEvent = false
      options = initDfp(options)
    }
    
    if (!listenEvent) {
      initIma3(options, player)
    } else {
      if( typeof $script === 'function' ) {
        $script.ready('gpt', function(){
          options = initDfp(options)
          initIma3(options, player)
        })
      }
    }
  }

}

function adTagFunc(options){
  const player = this
  player.on('loadstart', () => {
    onPlayerReady(player, videojs.mergeOptions(defaults, options));
  })
}

videojs.plugin('adTag', adTagFunc);

