const path = require('path')

var postCssConfig = ( file, options, env ) => ({
  plugins: [
    require('postcss-import')({
      path: path.resolve(__dirname, 'dist/css') 
    }),
    require('postcss-cssnext')({
      browsers: ['> 1%', 'last 2 versions', 'Firefox ESR', 'Opera 12.1']
    })
  ]}
)


module.exports = postCssConfig
