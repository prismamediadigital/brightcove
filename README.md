**Plugins Brightcove pour TéléLoisirs**

# Get started

### Installation

```
#!bash
$ npm install
```

### Build

```
#!bash
$ gulp
```

This command will read files in ./src and generate files in ./dist, commit this directory after each build.

### Using in Brightcove Studio
Put the RAW URL of the plugin that you want to use in Plugins/Javascript section in Brightcove as in the screenshot. (Files are stored in ./dist/)

If you want the plugin to be executed during initialisation of videojs, you need to declare an empty entry in "Name, Options" section, with name for the name of the plugin and "{}" for the option.

**JS section**

![Alt text](/screenshots/plugins.png?raw=true "Optional Title")

**Option section**

![Alt text](/screenshots/options.png?raw=true "Optional Title")

# Plugins

### Logo

File name: logo.js

Options:

```
#!json
{
    "logo": "base64 of the image [default: TL logo]",
    "debug": "debug mode [default: false]"
}
```

### Google Analytics

File name: ga.js

Options:

```
#!json
{
    "eventLabel": "[default: false => 'video ID | video title']",
    "percentsPlayedInterval": "[default: 10]",
    "tracker": "Google Analytics ID in case of embed [default: '']",
    "debug": "debug mode [default: false]"
}
```

Events To Track: look in defaults.eventsToTrack

### Secret Media

File name: sm.js

Options:

```
#!json
{
    "key": "Key of Secret media service [default: empty]",
    "url": "URL of VAST to be unblocked [default: empty]",
    "debug": "debug mode [default: false]"
}
```

### Scroll Into View

File name: scrollIntoView.js

Options: nothing

Dependancies: jQuery and prismamediadigital/pmd-isonscreen
