/**
 * Plugin to initialize sticky scrolling
 * Uses config in JSON format
 * Example :
 * {
        debug: {
          href: 'http://localhost:8000'
        },
        position: { // define for which type of device wiich configuration to use
          desktop: { // for desktop
            dimensions: '500x250', // dimension of the player when sticky, can be 'fluid' in this case player take full width and keep ratio for height
            right: 0, // where to fix player from right of the screen
            bottom: 0 // and from bottom
          },
          mobile: { // for mobile device
            dimensions: '300x150',
            top: 0 // because mobile player are allmost near full width, the only are param are from top
          }
        }
    }
 * The plugin must be initialized in brightcove backoffice
 */
// import videoJS
import videojs from 'videojs'
// import any external depenency
import scrollMonitor from 'scrollmonitor'
// import Style that are needed
import '../css/sticky.css'

let adsStatus = false
let activePlay = false
let stickyStatus = false

let playerBaseDimensions = window.playerBaseDimensions || {}

function findGetParameterValue (parameterName, parameterValue) {
  let result = false
  let tmp = []
  location.search
    .substr(1)
    .split('&')
    .forEach(function (item) {
      tmp = item.split('=')
      if (tmp[0] === parameterName) {
        if (tmp[1] === parameterValue) {
          result = true
        }
      }
    })
  return result
}

function createDomEl (id, styles, cb) {
  let elem = document.createElement('div')
  elem.setAttribute('id', id)
  if (typeof styles !== 'undefined') {
    elem.setAttribute('style', styles)
  }
  if (typeof cb === 'function') {
    cb(elem)
  }
  return elem
}

function getParentZindex (target) {
  let els = []
  while (target) {
    els.unshift(target)
    target = target.parentNode
    if (target.id !== 'corps' && target.nodeName !== 'BODY') {
      const targetStyle = document.defaultView.getComputedStyle(target, null)
      const layer = targetStyle.getPropertyValue('z-index')
      if (layer <= 1000) {
        target.style.zIndex = 'auto'
      }
    } else { return }
  }
}

function getRatio (baseDimesions) {
  const baseRatio = baseDimesions.width / baseDimesions.height

  return baseRatio
}

function getRefixeStatus () {
  const refixe = document.getElementById('refixePlayer')
  if ((adsStatus === true || activePlay === true) && stickyStatus === true) {
    refixe.style.display = 'block'
  } else {
    refixe.style.display = 'none'
  }
}

function getCurrentPlayer () {
  var getPlayerList = videojs.getPlayers()
  var getCurrentPlayer = getPlayerList[Object.keys(getPlayerList)[Object.keys(getPlayerList).length - 1]]

  return getCurrentPlayer
}

function checkPlayerDimensions (player) {
  let playerWidth
  let playerHeight

  if (stickyStatus === true) {
    const wrapper = document.getElementById('wrapperPlayer')
    playerWidth = wrapper.clientWidth
    playerHeight = wrapper.clientHeight
  } else {
    playerWidth = playerBaseDimensions.width
    playerHeight = playerBaseDimensions.height
  }

  player.width(playerWidth)
  player.height(playerHeight)
}

function refixePosition (elem, position) {
  if (position === 'top-left') {
    elem.style.top = '-25px'
    elem.style.left = 0
  }

  if (position === 'top-right') {
    elem.style.top = '-25px'
    elem.style.right = 0
  }

  if (position === 'bottom-left') {
    elem.style.bottom = '-25px'
    elem.style.left = 0
  }

  if (position === 'bottom-right') {
    elem.style.bottom = '-25px'
    elem.style.right = 0
  }
}

function createHTML (elem) {
  const getPlayerParent = elem.parentElement
  // const clonePlayer = elem.cloneNode(true)
  const wrapPlayer = createDomEl('wrapperPlayer')
  const wrapTittle = createDomEl('wrapperTittle', 'display:none;')
  const fixePlayer = createDomEl('refixePlayer', 'display:none;', (element) => {
    element.innerHTML += 'Fermer'
  })

  console.log('>>> elem :', elem)
  console.log('>>> getPlayerParent :', getPlayerParent)
  console.log('>>> wrapPlayer :', wrapPlayer)

  getPlayerParent.appendChild(wrapPlayer)
  wrapPlayer.appendChild(fixePlayer)
  // wrapPlayer.appendChild(wrapTittle)
  wrapPlayer.appendChild(elem)
}

function stickyness (wrapper, refixe, config) {
  const ratio = getRatio(playerBaseDimensions)
  let splitDim

  wrapper.style.position = 'fixed'
  wrapper.style.zIndex = 100100

  if (window.innerWidth > 768) { // is desktop
    splitDim = config.position.desktop.dimensions.split('x')

    wrapper.style.right = `${config.position.desktop.right}px`
    wrapper.style.bottom = `${config.position.desktop.bottom}px`

    refixePosition(refixe, config.position.desktop.refixe)
  } else { // if not desktop, it's mobile
    splitDim = config.position.mobile.dimensions.split('x')

    wrapper.style.top = config.position.mobile.top

    refixePosition(refixe, config.position.mobile.refixe)
  }

  if (splitDim[0] !== 'fluid') {
    wrapper.style.width = `${splitDim[0]}px`
    wrapper.style.height = `${splitDim[1]}px`
  } else {
    wrapper.style.width = '100%'
    wrapper.style.height = `${window.innerWidth / ratio}px`
    wrapper.style.left = '0'
  }

  // const player = getCurrentPlayer()

  // console.log('wrapper:', wrapper.clientWidth)
  // console.log('player :', player)

  // player.width(wrapper.style.width)
  // player.height(wrapper.style.height)

  stickyStatus = true

  getRefixeStatus()
}

function unStickyness (wrapper, refixe, config) {
  // wrapper.style = ''
  wrapper.setAttribute('style', '')

  // refixe.style.display = 'none'

  // player.width(playerBaseDimension.width)
  // player.height(playerBaseDimension.height)

  stickyStatus = false

  getRefixeStatus()
}

function watching (player, config) {
  const wrapper = document.getElementById('wrapperPlayer')
  const refixe = document.getElementById('refixePlayer')
  const wrapperParent = wrapper.parentElement

  let elementWatcher = scrollMonitor.create(wrapperParent, -playerBaseDimensions.height * 0.5)
  elementWatcher.lock()

  console.log('Watching !!', wrapperParent)

  refixe.addEventListener('click', (event) => {
    if (event.target && event.target.nodeName === 'DIV') {
      elementWatcher.destroy()
      unStickyness(wrapper, refixe, config)
      checkPlayerDimensions(player)
    }
  })

  elementWatcher.enterViewport(function () {
    console.log('Enter Viewport')
    unStickyness(wrapper, refixe, config)
  })

  elementWatcher.exitViewport(function () {
    console.log('Exit Viewport')
    // if (!elementWatcher.isBelowViewport) {
    //   console.log('is Below Viewport')
    //   stickyness(wrapper, refixe, config)
    // }
    stickyness(wrapper, refixe, config)
  })

  stickyness(wrapper, refixe, config)

  setInterval(() => {
    elementWatcher.recalculateLocation()
  }, 2000)
}

function init (player, config) {
  // initSticky = true
  if (!document.getElementById('wrapperPlayer')) {
    const getPlayer = document.getElementById(player.id_)
    playerBaseDimensions = {'height': player.currentHeight(), 'width': player.currentWidth()}

    window.playerBaseDimensions = playerBaseDimensions

    console.log('playerBaseDimensions :', playerBaseDimensions, window.playerBaseDimensions )

    getParentZindex(getPlayer)
    createHTML(getPlayer)
  }

  // document.getElementById('wrapperTittle').innerHTML = player.mediainfo.name

  checkPlayerDimensions(player)

  getRefixeStatus()

  // watching(config)
  // if (scrollMonitor.watchers.length === 0) {
  //   watching(player, config)
  // }
}

function isPlayTel () {
  var regex = /(.*)\.programme-tv.(net|local)\/videos\//
  return regex.test(document.location.href)
}

function stickyPlayer (options) {
  // if (options.debug) {
  //   const windowLocation = window.location.hostname === 'dev.femmeactuelle.fr' || new RegExp(options.debug.href).test(window.location.pathname)
  //   if (!windowLocation) return
  // }

  const player = getCurrentPlayer()
  const config = options
  let deviceFlag = false

  // console.log('player :', player)

  if (!findGetParameterValue('typetag', 'amp') && !isPlayTel()) {
    if (window.innerWidth > 768) { // is desktop
      if (config.position.desktop) {
        deviceFlag = true
      }
    } else { // not desktop, so mobile
      if (config.position.mobile) {
        deviceFlag = true
      }
    }
  }

  if (deviceFlag) {
    player.ready(() => {
      init(player, config)
      player.one('play', () => {
        console.log('Player Play !!')
        watching(player, config)
      })

      player.one('playing', () => {
        console.log('Player Playing !!')
        activePlay = true
        watching(player, config)
      })

      player.one('ads-first-quartile', () => {
        console.log('Ads First Quartile !!')
        adsStatus = true
        getRefixeStatus()
      })
    })
  }
}
videojs.registerPlugin('stickyPlayer', stickyPlayer)
