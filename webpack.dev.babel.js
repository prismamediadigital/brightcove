import webpack from 'webpack'
import path from 'path'

const configWebpackDev = {
  devtool: '#inline-source-map',
  context: path.resolve(__dirname, 'app'),
  entry: {
    videojs: './js/videojs.js', // used only for local/dev
    autoplay: './js/autoplay.js',
    sticky: './js/sticky.js',
    ga: './js/ga.js',
    charity: './js/charity.js',
    logo: './js/logo.js',
    social: './js/initSocial.js',
    unmute: './js/unmuteDesktop.js',
    scroll2play: './js/scrollIntoView.js',
    i18nFR: './js/i18n/fr.js'
  },
  output: {
    filename: 'js/[name]-plugin.js',
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/',
    jsonpFunction: 'pmBrightcovePlugins',
    library: ['pmBrightcove', '[name]'],
    libraryTarget: 'umd',
    libraryExport: 'default'
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /(node_modules|bower_components)/,
      use: {
        loader: 'babel-loader'
      }
    },
    {
      test: /\.css$/,
      exclude: /(node_modules|bower_components)/,
      use: [
        'style-loader',
        { loader: 'css-loader', options: { importLoaders: 1 } },
        'postcss-loader'
      ]
    }]
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify(process.env.NODE_ENV)
      }
    }),
    new webpack.optimize.ModuleConcatenationPlugin()
  ],
  resolve: {
    modules: ['node_modules'],
    extensions: ['.js', '.css']
  },
  externals: {
    googletag: 'googletag',
    'googletag.cmd': 'googletag.cmd',
    pmdAdvertisingConfig: 'pmdAdvertisingConfig',
    dataLayer: 'dataLayer',
    gtmDataLayer: 'gtmDataLayer',
    videojs: 'videojs'
  },
  devServer: {
    contentBase: [path.resolve(__dirname, './__mocks__/')],
    host: 'dev.programme-tv.net',
    compress: true,
    port: 9000
  }
}

export default configWebpackDev
