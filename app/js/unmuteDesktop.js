import videojs from 'videojs'

function unMuteDesktop(options){
  const player = this,
        playerEl = player.el();

  player.on('loadstart', () => {

    if( window.innerWidth > 1024 ){
      var videoTag = playerEl.getElementsByTagName('video')[0];

      videoTag.muted = false;
      player.muted(false);
      if (options.debug) {
        console.info("unMuteDesktop : after unMuted -> videoTag = ", videoTag);
      }
    }

  })
}

videojs.registerPlugin('unMuteDesktop', unMuteDesktop)
