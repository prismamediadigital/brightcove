import videojs from 'videojs'

import autoPlayProm from './autoplay'

// Default options for the plugin.
const defaults = {
  checkOnReady: false,
  triggerOnce: true,
  count: 0,
  debug: false
}

let init = []
const onPlayerReady = function (player, options) {
  options = videojs.mergeOptions(defaults, options || {})

  let timer
  let listener = function () {
    // Trigger 250ms after when finish scrolling and only once
    clearTimeout(timer)
    timer = setTimeout(function () {
      checkIfVideoInView()
    }, 250)
  }

  let isInViewport = function (element) {
    if (typeof element === 'string') {
      element = document.getElementById(element)
    }

    var rect = element.getBoundingClientRect()
    var html = document.documentElement

    if (options.debug) { 
      videojs.log(
        'rect', rect,
        'html.clientHeight', html.clientHeight,
        'html.clientWidth', html.clientWidth,
        'window.innerHeight', window.innerHeight,
        'window.innerWidth', window.innerWidth
      )
    }

    return (
      rect.top >= 0 &&
      rect.left >= 0 &&
      rect.bottom <= (window.innerHeight || html.clientHeight) &&
      rect.right <= (window.innerWidth || html.clientWidth)
    )
  }

  let triggerOnceCheckFunc = function () {
    if (options.triggerOnce || options.count !== 0) {
      if (options.debug) {
        videojs.log('Player ' + bcPlayerId + '(#' + player.id_ + ') > Plugin scrollIntoView, remove listener')
      }

      window.removeEventListener('scroll', listener)
      player.off('play', triggerOnceCheckFunc)
    }
  }

  player.on('play', triggerOnceCheckFunc)

  let checkIfVideoInView = function () {
    let isInView = isInViewport(player.id_)
    if (options.debug) {
      videojs.log('Player ' + bcPlayerId + '(#' + player.id_ + ') > Plugin scrollIntoView, player is in view', isInView)
    }
    // Player is fully in viewport, is never played and is in pause
    if (isInView && options.count === 0) {
      autoPlayProm(player)
      options.count++
    }
  }

  let bcPlayerId = player.el_.getAttribute('data-player')

  if (init[player.id_] !== undefined) {
    if (options.debug) {
      videojs.log('Player ' + bcPlayerId + '(#' + player.id_ + ') > Plugin scrollIntoView, already initialized, skip.')
    }
    return
  } else {
    if (options.debug) {
      videojs.log('Player ' + bcPlayerId + '(#' + player.id_ + ') > Plugin scrollIntoView', options)
    }
    init[player.id_] = true
  }

  window.addEventListener('scroll', listener)

  if (options.checkOnReady) {
    player.one('loadedmetadata', function () {
      checkIfVideoInView()
    })
  }
}

const scrollIntoViewFunc = function (options) {
  this.ready(() => {
    onPlayerReady(this, options)
  })
}

videojs.registerPlugin('scrollIntoView', scrollIntoViewFunc)
